export class Product{

    id_product	    : number;
    name            : string;
    totalQty	    : number;
    remaining	    : number;
    id_warehouse    : number;

}