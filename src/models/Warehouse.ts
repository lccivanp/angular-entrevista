export class Warehouse{

    id_warehouse : number;
    name: string;
    min_product : number;
    max_product : number;
}