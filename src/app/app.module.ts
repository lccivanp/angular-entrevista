import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { MatTableModule, MatSelectModule } from '@angular/material';
import { AppComponent } from './app.component';
import { InventoryComponent } from './components/inventory/inventory.component';
import { RouterModule, Routes } from '@angular/router';

import {MatExpansionModule ,MatButtonModule,MatFormFieldModule, MatOptionModule} from '@angular/material/';


import { WarehouseService } from './services/warehouse.service';
import { ProductService } from './services/product.service';


const routes: Routes = [
  {

      path: '',
      component: InventoryComponent
    
  }
];

@NgModule({
  declarations: [
    AppComponent,
    InventoryComponent,
  
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes,{ useHash: true }),
    HttpClientModule,
    MatTableModule,
    MatExpansionModule, 
    MatButtonModule,
    MatFormFieldModule, 
    MatOptionModule,
    MatSelectModule
  ],
  providers: [WarehouseService,ProductService],
  bootstrap: [AppComponent]
})
export class AppModule { }
