import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
//import {Observable} from "rxjs/Observable";
import {Observable} from 'rxjs/Observable';
import { Warehouse } from 'src/models/Warehouse';

interface myResponse {
  error: String,
  data : Warehouse
  message:String
}

interface myResponses {
  error: String,
  data : Warehouse[]
  message:String
}

@Injectable()
export class WarehouseService {

  constructor(private _http: HttpClient) { }

  getWarehouseById(idProduct):Observable<myResponse>{
    const serviceURL = "http://localhost:8080/warehouse/"+idProduct
    return this._http.get<myResponse>(serviceURL);
  }

  getWarehouses():Observable<myResponses>{
    const serviceURL = "http://localhost:8080/warehouse/"
    return this._http.get<myResponses>(serviceURL);
  }
}
