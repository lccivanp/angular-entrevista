import { Injectable } from '@angular/core';
import { Product } from 'src/models/Product';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

interface myResponse {
  error: String,
  data : Product
  message:String
}

interface myResponses {
  error: String,
  data : Product[]
  message:String
}


@Injectable()
export class ProductService {

  constructor(private _http: HttpClient) { }

  getProductById(idProduct):Observable<myResponse>{
    const serviceURL = "http://localhost:8080/product/"+idProduct
    return this._http.get<myResponse>(serviceURL);
  }

  getProducts():Observable<myResponses>{
    const serviceURL = "http://localhost:8080/product/"
    return this._http.get<myResponses>(serviceURL);
  }
}
