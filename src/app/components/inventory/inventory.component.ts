import { Component, OnInit } from '@angular/core';
import { WarehouseService } from 'src/app/services/warehouse.service';
import { Warehouse } from 'src/models/Warehouse';
import { Product } from 'src/models/Product';
import { ProductService } from 'src/app/services/product.service';



@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.css']
})
export class InventoryComponent implements OnInit {
  displayedColumns: string[] = [ "Product Name", "On Stock", "Sold" ,"Total" ,"Warehouse" ];
  warehouses: Warehouse[] =[]
  warehouse: Warehouse

  products: Product[] =[]
  product: Product

  panelOpenState = false;

  constructor(private warehouseService : WarehouseService,
            private productService : ProductService
    ) { }

  ngOnInit() {
    this.warehouseService.getWarehouses().subscribe(
      (val) => {
        console.log(val.data)
        this.warehouses = val.data
      },
      err => {
         
      },
      () => {
          
      }
    )

    this.productService.getProducts().subscribe(
      (val) => {
        console.log(val.data)
        this.products = val.data
      },
      err => {
      },
      () => {
          
      }
    )

      

  }

  showWarehouse(id_warehouse){
    let name;
    for (let i of this.warehouses) {
      if(id_warehouse == i.id_warehouse){
        name = i.name;
      }
   }
   return name;
  }
}
